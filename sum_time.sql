﻿select
	q.id,
	q.type,
	sum(a.time) as answer_time,
	avg(a.time) as average_time,
	count(*) as correct_count,
	wrong_count,
	wrong_count * 5 * 1000 as penalty,
	sum(a.time) + wrong_count * 5 * 1000 as sum_time,
	(sum(a.time) + wrong_count * 5 * 1000) / count(*) as final_average
from
	questions q
left join answers a on q.id = a.question_id
left join
	(select
		a1.question_id as q_id,
		count(*) as wrong_count
	from
		answers a1,
		questions q1
	where
		a1.question_id = q1.id
		and ((q1.answer = true and a1.result != 1)
		or (q1.answer = false and a1.result != 2))
	group by
		a1.question_id) w
	on q.id = w.q_id
where 
	a.poll_id in (select id from polls where question_id = 11)
group by
	q.id, wrong_count;