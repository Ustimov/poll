﻿select
	q.type,
	avg(a.time)
from
	answers a,
	questions q
where 
	a.poll_id in (select id from polls where question_id = 11)
	and a.question_id = q.id
group by
	q.id;