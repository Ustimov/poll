import React from 'react';
import Chart from 'chart.js';
import './ControlChart.css';

let ChartType = {
    UNDEFINED: 0,
    SIMPLE: 1,
    FILL: 2,
    LINE: 3,
    FILL_LINE: 4
};

class ControlChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }
    componentDidMount() {
        let ctx = document.getElementById('canvas').getContext('2d');
        this.chart = new Chart(ctx, {
            type: 'line',
            responsive: true,
            maintainAspectRatio: false,
            options: {
                tooltips: {
                    enabled: false
                },
                hover: {
                    mode: null,
                },
                legend: {
                    labels: {
                        filter: (legendItem, chartData) => legendItem.text
                    }
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            min: this.state.date,
                            // A new point every 10 seconds (total 7 points)
                            max: new Date(this.state.date.getTime() + 7 * 10000),
                            stepSize: 10,
                            displayFormats: {
                                second: 'hh:mm:ss'
                            }
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            min: 0,
                            max: 20
                        }
                    }]
                }
            }
        });
    }
    redraw() {
        this.chart.data.datasets.length = 0;
        let startDate = new Date(this.state.date.getTime() + 10000);
        let processDatasets = [[]];
        let lowerControlLimitDataSet = [];
        let upperControlLimitDataSet = [];
        lowerControlLimitDataSet.push({
            x: this.state.date,
            y: this.props.lowerControlLimit
        });
        upperControlLimitDataSet.push({
            x: this.state.date,
            y: this.props.upperControlLimit
        });
        for (let i = 0; i < this.props.points.length; i++) {
            let currentDate = new Date(startDate.getTime() + i * 10000);
            processDatasets[0].push({
                x: currentDate,
                y: this.props.points[i]
            });
            lowerControlLimitDataSet.push({
                x: currentDate,
                y: this.props.lowerControlLimit
            });
            upperControlLimitDataSet.push({
                x: currentDate,
                y: this.props.upperControlLimit
            });
        }
        let fill = false;
        let borderColor = 'black';
        if (this.props.type === ChartType.FILL || this.props.type === ChartType.FILL_LINE) {
            fill = "+1";
            borderColor = 'transparent';
        }
        if (this.props.type === ChartType.LINE || this.props.type === ChartType.FILL_LINE) {
            this.splitProcessDataset(processDatasets);
        }
        let datasets = [
            {
                data: upperControlLimitDataSet,
                fill: fill,
                pointRadius: 0,
                borderColor: borderColor,
                borderDash: [10, 10],
                borderWidth: 1
            },
            {
                data: lowerControlLimitDataSet,
                fill: false,
                pointRadius: 0,
                borderColor: borderColor,
                borderDash: [10, 10],
                borderWidth: 1
            }
        ];
        for (let k = 0; k < processDatasets.length; k++) {
            let borderColor =
                (processDatasets.length > 1 &&
                processDatasets[k][processDatasets[k].length - 1].y - processDatasets[k][processDatasets[k].length - 2].y < 0) ?
                    'blue' : 'red';
            datasets.push({
                data: processDatasets[k],
                fill: false,
                pointRadius: 2,
                lineTension: 0,
                borderColor: borderColor,
                borderWidth: 1
            });
        }
        for (let j = 0; j < datasets.length; j++) {
            this.chart.data.datasets.push(datasets[j]);
        }
        this.chart.update();
    }
    splitProcessDataset(processDatasets) {
        let processDataset = processDatasets[0];
        processDatasets.length = 0;
        let newDataset = [];
        for (let i = 0; i < processDataset.length; i++) {
            if (newDataset.length > 1) {
                let previousIncline = newDataset[newDataset.length - 1].y - newDataset[newDataset.length - 2].y;
                let currentIncline = processDataset[i].y - newDataset[newDataset.length - 1].y;
                if ((previousIncline > 0 && currentIncline < 0) || (previousIncline < 0 && currentIncline > 0)) {
                    processDatasets.push(newDataset);
                    newDataset = [newDataset[newDataset.length - 1]];
                }
            }
            newDataset.push(processDataset[i]);
        }
        processDatasets.push(newDataset);
    }
    render() {
        return (
            <div>
                <div className="ControlChart-chart-container">
                    <canvas id="canvas" width="600" height="300" />
                </div>
            </div>
        );
    }
}

export default ControlChart;