import React from 'react';
import ControlChart from './ControlChart';

class Questions extends React.Component {
    constructor(props) {
        super(props);
        this.selectText = 'Выберите вариант ответа:';
        this.nextText = 'Ответ принят';
        this.errorText = 'Во время отправки ответа произошла ошибка. Повторите ещё раз';
        this.state = {
            helpText: this.selectText,
            isAnswerSelected: false,
            isNotAnswered: true,
            isWaitingForNext: false
        }
    }
    nextQuestion() {
        this.setState({
            helpText: this.selectText,
            isNotAnswered: true,
            isWaitingForNext: false
        });
        if (this.state.isLast) {
            this.props.endHandler();
            return;
        }
        fetch('/api/question?poll_id=' + this.state.pollId)
            .then(response => response.json())
            .then(json => this.updateStateWithJson(json));
    }
    applyAnswer(e) {
        e.preventDefault();
        let now = new Date();
        fetch('/api/answer', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                poll_id: this.state.pollId,
                question_id: this.state.id,
                result: this.yes.checked ? 1 : 2,
                time: now - this.state.begin
            })
        }).then(response => {
            this.yes.checked = false;
            this.no.checked = false;
            this.setState({
                helpText: this.nextText,
                isAnswerSelected: false,
                isNotAnswered: false,
                isWaitingForNext: true
            })
        }).catch(e => {
            this.setState({helpText: this.errorText});
        });
    }
    createNewPoll() {
        fetch('/api/question')
            .then(response => response.json())
            .then(json => this.updateStateWithJson(json));
    }
    updateStateWithJson(json) {
        this.setState({
            id: json['id'],
            pollId: json['poll_id'],
            isLast: json['is_last'],
            type: json["type"],
            lowerControlLimit: json['lower_control_limit'],
            upperControlLimit: json['upper_control_limit'],
            points: json['points'],
            begin: new Date()
        });
        this.chart.redraw();
    }
    answerSelected() {
        this.setState({isAnswerSelected: true});
    }
    render() {
        return (
            <div className={this.props.visible ? "" : "hidden"}>
                <h1>Опрос</h1>
                <p>График {this.state.id} из 10</p>
                <ControlChart {...this.state} ref={chart => this.chart = chart} />
                <form>
                    <p>Процесс под контролем?</p>
                    <p>{this.state.helpText}</p>
                    <p><input onChange={this.answerSelected.bind(this)} ref={yes => this.yes = yes} disabled={this.state.isWaitingForNext} name="answer" type="radio" value="y" />Да</p>
                    <p><input onChange={this.answerSelected.bind(this)} ref={no => this.no = no} disabled={this.state.isWaitingForNext} name="answer" type="radio" value="n" />Нет</p>
                    <button onClick={this.applyAnswer.bind(this)} disabled={!this.state.isAnswerSelected || this.state.isWaitingForNext} type="submit">Ответить</button>
                </form>
                <button onClick={this.nextQuestion.bind(this)} disabled={this.state.isNotAnswered}>Далее</button>
            </div>
        );
    }
}

export default Questions;