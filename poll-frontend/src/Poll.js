import React from 'react';
import Instructions from './Instructions';
import Questions from './Questions';
import Thanks from './Thanks';

class Poll extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showInstructions: true,
            showQuestions: false,
            showThanks: false
        };
    }
    startPoll() {
        this.questions.createNewPoll();
        this.setState({
            showInstructions: false,
            showQuestions: true
        });
    }
    endPoll() {
        this.setState({
            showQuestions: false,
            showThanks: true
        });
    }
    backToInstructions() {
        this.setState({
            showInstructions: true,
            showThanks: false
        });
    }
    render() {
        return (
            <div>
                <Instructions
                    visible={this.state.showInstructions}
                    startHandler={this.startPoll.bind(this)} />
                <Questions
                    visible={this.state.showQuestions}
                    endHandler={this.endPoll.bind(this)}
                    ref={questions => this.questions = questions} />
                <Thanks
                    visible={this.state.showThanks}
                    closeHandler={this.backToInstructions.bind(this)} />
            </div>
        );
    }
}

export default Poll;