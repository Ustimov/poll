import React from 'react';
import Poll from './Poll';

class App extends React.Component {
    render() {
        return (
            <div>
                <Poll />
            </div>
        );
    }
}

export default App;
