import React from 'react';

class Thanks extends React.Component {
    render() {
        return (
            <div className={this.props.visible ? "" : "hidden"}>
                <h1>Спасибо!</h1>
                <button onClick={this.props.closeHandler}>Закрыть</button>
            </div>
        );
    }
}

export default Thanks;