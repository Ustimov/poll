--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: answers; Type: TABLE; Schema: public; Owner: poll
--

CREATE TABLE answers (
    poll_id character varying(255),
    question_id integer,
    result integer,
    id integer NOT NULL,
    "time" integer
);


ALTER TABLE answers OWNER TO poll;

--
-- Name: answers_id_seq; Type: SEQUENCE; Schema: public; Owner: poll
--

CREATE SEQUENCE answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE answers_id_seq OWNER TO poll;

--
-- Name: answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: poll
--

ALTER SEQUENCE answers_id_seq OWNED BY answers.id;


--
-- Name: polls; Type: TABLE; Schema: public; Owner: poll
--

CREATE TABLE polls (
    id character varying(255),
    question_id integer
);


ALTER TABLE polls OWNER TO poll;

--
-- Name: qid_seq; Type: SEQUENCE; Schema: public; Owner: poll
--

CREATE SEQUENCE qid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE qid_seq OWNER TO poll;

--
-- Name: questions; Type: TABLE; Schema: public; Owner: poll
--

CREATE TABLE questions (
    id integer DEFAULT nextval('qid_seq'::regclass) NOT NULL,
    type integer,
    is_last boolean,
    ucl integer,
    lcl integer,
    points character varying(255),
    next_question integer,
    answer boolean
);


ALTER TABLE questions OWNER TO poll;

--
-- Name: answers id; Type: DEFAULT; Schema: public; Owner: poll
--

ALTER TABLE ONLY answers ALTER COLUMN id SET DEFAULT nextval('answers_id_seq'::regclass);


--
-- Data for Name: answers; Type: TABLE DATA; Schema: public; Owner: poll
--

COPY answers (poll_id, question_id, result, id, "time") FROM stdin;
\.


--
-- Name: answers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: poll
--

SELECT pg_catalog.setval('answers_id_seq', 221, true);


--
-- Data for Name: polls; Type: TABLE DATA; Schema: public; Owner: poll
--

COPY polls (id, question_id) FROM stdin;
\.


--
-- Name: qid_seq; Type: SEQUENCE SET; Schema: public; Owner: poll
--

SELECT pg_catalog.setval('qid_seq', 2, true);


--
-- Data for Name: questions; Type: TABLE DATA; Schema: public; Owner: poll
--

COPY questions (id, type, is_last, ucl, lcl, points, next_question, answer) FROM stdin;
2	1	f	6	14	[12,11,10,9,9.1,8,7]	3	t
4	2	f	5	14	[13, 12, 11, 10, 9, 8, 7]	5	f
5	2	f	7	15	[8, 9, 10.1, 10, 11, 12, 13]	6	t
6	2	f	6	14	[15, 13, 11, 9, 10, 12, 8]	7	f
7	3	f	8	16	[14,13,12,11,11.1,10,9]	8	t
8	3	f	7	15	[9,13,11,10,12,14,16]	9	f
9	4	f	7	15	[8, 9, 10.1, 10, 11, 12, 13]	10	t
10	4	t	6	14	[15, 13, 11, 9, 10, 12, 8]	11	f
1	1	f	4	13	[6,7,8,9,10,11,12]	2	f
3	1	f	5	13	[7,11,9,8,10,12,14]	4	f
\.


--
-- Name: answers answers_pkey; Type: CONSTRAINT; Schema: public; Owner: poll
--

ALTER TABLE ONLY answers
    ADD CONSTRAINT answers_pkey PRIMARY KEY (id);


--
-- Name: questions questions_pkey; Type: CONSTRAINT; Schema: public; Owner: poll
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

