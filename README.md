This is simple poll for Ergonomics course in BMSTU.

### Getting started

Run:

```
sudo docker-compose up -d
```

Stop:

```
sudo docker-compose stop
```

Restore data from dump:

```
sudo docker-compose exec postgres /src/restore-from-dump.sh
```

Rebuild frontend:

```
sudo docker-compose run node /src/build-frontend.sh
```

Make dump:

```
sudo docker-compose exec postgres pg_dump --username=poll poll > dump.sql
```

### License

MIT