create table polls(id varchar(255), question_id int);
create table answers(poll_id varchar(255), question_id int, result int);
create table questions(id int, type int, is_last bool, ucl int, lcl int, points varchar(255), next_question int);
alter table answers add column id int primary key serial;
create sequence qid_seq;
alter table questions alter column id set default nextval('qid_seq');
alter table answers add column time int;
alter table questions add column answer bool;