package main

import (
	"fmt"
	"net/http"
	"encoding/json"
	"database/sql"

	_ "github.com/lib/pq"
	"log"
	"time"
)

type Server struct {
	db *sql.DB
}

type Poll struct {
	Id string
	CurrentQuestionId int
}

type QuestionType int

const (
	UNDEFINED QuestionType = iota
	SIMPLE
	FILL
	LINE
	FILL_LINE
)

type Question struct {
	Id int `json:"id"`
	PollId string `json:"poll_id"`
	Type QuestionType `json:"type"`
	IsLast bool `json:"is_last"`
	UpperControlLimit int `json:"upper_control_limit"`
	LowerControlLimit int `json:"lower_control_limit"`
	Points []float32 `json:"points"`
	NextQuestionId int `json:"-"`
	Answer bool `json:"-"`
}

type AnswerType int

const (
	NONE AnswerType = iota
	YES
	NO
)

type Answer struct {
	PollId string `json:"poll_id"`
	QuestionId int `json:"question_id"`
	Result AnswerType `json:"result"`
	Time int `json:"time"`
}

func (s *Server) handleQuestion(w http.ResponseWriter, r *http.Request)  {
	pollId := r.URL.Query().Get("poll_id")
	p, err := getPoll(s.db, pollId)
	if err != nil {
		http.Error(w, "Can't get a poll", http.StatusInternalServerError)
		return
	}
	q, err := getQuestion(s.db, p.CurrentQuestionId)
	q.PollId = p.Id
	if err != nil {
		http.Error(w, "Can't get a question", http.StatusInternalServerError)
		return
	}
	bytes, err := json.Marshal(q)
	if (err != nil) {
		http.Error(w, "Can't marshal a question", http.StatusInternalServerError)
		return
	}
	w.Write(bytes)
	p.CurrentQuestionId = q.NextQuestionId
	err = updatePoll(s.db, *p)
	if (err != nil) {
		http.Error(w, "Can't update the poll", http.StatusInternalServerError)
		return
	}
}

func (s *Server) handleAnswer(w http.ResponseWriter, r *http.Request) {
	if (r.Method != "POST") {
		return;
	}
	decoder := json.NewDecoder(r.Body)
	var answer Answer
	if err := decoder.Decode(&answer); err != nil || answer == (Answer{}) {
		http.Error(w, "Wrong request body", http.StatusBadRequest)
		return
	}
	err := createAnswer(s.db, answer)
	if err != nil {
		http.Error(w, "Can't create an answer", http.StatusInternalServerError)
	}
}

func updatePoll(db *sql.DB, p Poll) error {
	stmt, err := db.Prepare("UPDATE polls SET question_id = $1 WHERE id = $2")
	defer stmt.Close()
	if err != nil {
		return err
	}
	_, err = stmt.Exec(p.CurrentQuestionId, p.Id)
	if err != nil {
		return err
	}
	return nil
}

func getPoll(db *sql.DB, pollId string) (*Poll, error) {
	if pollId != "" {
		stmt, err := db.Prepare("SELECT * FROM polls WHERE id = $1")
		defer stmt.Close()
		if err != nil {
			return nil, err
		}
		row := stmt.QueryRow(pollId)
		if row == nil {
			return nil, err
		}
		var p Poll
		row.Scan(&p.Id, &p.CurrentQuestionId)
		return &p, nil
	}
	p, err := createPoll(db)
	if err != nil {
		return nil, err
	}
	return p, nil
}

func createPoll(db *sql.DB) (*Poll, error)  {
	stmt, err := db.Prepare("INSERT INTO polls(id, question_id) VALUES($1, $2)")
	defer stmt.Close()
	if err != nil {
		return nil, err
	}
	pollIdString := time.Now().Format("2006-01-0215:04:05.999999")
	questionId, err := minQuestionId(db)
	if err != nil {
		return nil, err
	}
	_, err = stmt.Exec(pollIdString, questionId)
	if err != nil {
		return nil, err
	}
	p := Poll{Id: pollIdString, CurrentQuestionId: questionId}
	return &p, nil
}

func minQuestionId(db *sql.DB) (int, error) {
	stmt, err := db.Prepare("SELECT id FROM questions WHERE id = (SELECT min(id) FROM questions)")
	defer stmt.Close()
	if err != nil {
		return -1, err
	}
	row := stmt.QueryRow()
	if row == nil {
		log.Fatal("There is not questions")
	}
	id := -1
	err = row.Scan(&id)
	return id, err
}

func getQuestion(db *sql.DB, questionId int) (*Question, error) {
	stmt, err := db.Prepare("SELECT * FROM questions WHERE id = $1")
	defer stmt.Close()
	if err != nil {
		return nil, err
	}
	row := stmt.QueryRow(questionId)
	if row == nil {
		return nil, nil
	}
	var q Question
	var points string
	row.Scan(&q.Id, &q.Type, &q.IsLast, &q.UpperControlLimit, &q.LowerControlLimit, &points, &q.NextQuestionId, &q.Answer)
	json.Unmarshal([]byte(points), &q.Points)
	return &q, nil
}

func createAnswer(db *sql.DB, answer Answer) error {
	stmt, err := db.Prepare("INSERT INTO answers(poll_id, question_id, result, time) VALUES($1, $2, $3, $4)")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(answer.PollId, answer.QuestionId, answer.Result, answer.Time)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	db, err := sql.Open("postgres", "host=postgres user=poll password=poll dbname=poll sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	s := Server{db: db}
	http.HandleFunc("/api/question", s.handleQuestion)
	http.HandleFunc("/api/answer", s.handleAnswer)
	fmt.Print("Started running on http://*:7000\n")
	fmt.Println(http.ListenAndServe(":7000", nil))
}