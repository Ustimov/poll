﻿select
	a.poll_id,
	count(*) as wrong_answers
from
	answers a,
	questions q
where 
	a.poll_id in (select id from polls where question_id = 11)
	and a.question_id = q.id
	and ((q.answer = true and a.result != 1) or (q.answer = false and a.result != 2))
group by
	a.poll_id;